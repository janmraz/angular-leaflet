﻿declare module L
{
	export module Proj
	{
		//export module CRS
		//{
		//	//constructor(a, b, c, d);
		//	export class TMS
		//	{
		//		constructor(a, b, c, d);
		//	}
		//}

		export class CRS
		{
			constructor(a, b, c);
        }

	    export class GeoJSON {
            constructor(geoJson, options);
	    }
	}

	//export class tileLayer
	//{
	//	constructor(a);
	//}
}