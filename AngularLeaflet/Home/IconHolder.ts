﻿var LeafIcon = L.Icon.extend({
    options: {
        //iconSize: [38, 95],
        //shadowSize: [50, 64],
        //iconAnchor: [22, 94],
        //shadowAnchor: [4, 62],
        //popupAnchor: [-3, -76]
    }
});
var greenIcon = new LeafIcon({ iconUrl: "Content/images/leaf-green.png" }),
    police = new LeafIcon({ iconUrl: "Content/images/police.png", iconSize: [35, 35] }),
    ambulance = new LeafIcon({ iconUrl: "Content/images/emergeny.png", iconSize: [35, 35] }),
    firemen = new LeafIcon({ iconUrl: "Content/images/firemen.png", iconSize: [35, 35] }),
    hazardousWaste = new LeafIcon({ iconUrl: "Content/images/waste.png", iconSize: [35, 35] }),
    schelder = new LeafIcon({ iconUrl: "Content/images/ukrytSmall.png", iconSize: [35, 35] }),
    sirenRed = new LeafIcon({ iconUrl: "Content/images/sirenRed.png", iconSize: [35, 35] }),
    sirenGreen = new LeafIcon({ iconUrl: "Content/images/sirenGreen.png", iconSize: [35, 35] }),
    objectPolice = new LeafIcon({ iconUrl: "Content/images/objectpolice.png", iconSize: [35, 35] }),
    circle = new LeafIcon({ iconUrl: "Content/images/circle.png", iconSize: [16, 16], iconAnchor: [8, 8] }),
    greenMetro = new LeafIcon({ iconUrl: "Content/images/GreenMetro.png", iconSize: [35, 35]}),
    redMetro = new LeafIcon({ iconUrl: "Content/images/RedMetro.png", iconSize: [35, 35]}),
    yellowMetro = new LeafIcon({ iconUrl: "Content/images/YellowMetro.png", iconSize: [35, 35]}),
    greenEnter = new LeafIcon({ iconUrl: "Content/images/GreenEnter.png", iconSize: [35, 35]}),
    redEnter = new LeafIcon({ iconUrl: "Content/images/RedEnter.png", iconSize: [35, 35]}),
    yellowEnter = new LeafIcon({ iconUrl: "Content/images/YellowEnter.png", iconSize: [35, 35] }),
    stick = new LeafIcon({ iconUrl: "Content/images/Stick.png", iconSize: [1, 35] }),
    camera = new LeafIcon({iconUrl: "Content/images/kamera.png", iconSize: [35, 35]}),
    schelderBig = new LeafIcon({iconUrl: "Content/images/SchelderBig.png", iconSize: [35, 35]}),
    free = new LeafIcon({ iconUrl: "Content/images/free.png", iconSize: [1, 1]});

var icons =
    {
        greenIcon: greenIcon,
        police: police,
        ambulance: ambulance,
        firemen: firemen,
        hazardousWaste: hazardousWaste,
        schelder: schelder,
        sirenRed: sirenRed,
        sirenGreen: sirenGreen,
        objectPolice: objectPolice,
        circle: circle,
        redMetro: redMetro,
        yellowMetro: yellowMetro,
        greenMetro: greenMetro,
        redEnter: redEnter,
        yellowEnter: yellowEnter,
        greenEnter: greenEnter,
        schelderBig: schelderBig,
        stick: stick,
        camera: camera,
        free:free

    }
