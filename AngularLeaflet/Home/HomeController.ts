﻿/// <reference path="../Scripts/typings/leaflet/proj4leaflet.d.ts" />
/// <reference path="../Scripts/typings/leaflet/esri-leaflet.d.ts" />

module Intens.TestApp.Home
{
	export class HomeController
	{
		static $inject = ["$scope", "leafletData"];

		constructor($scope: IHomeScope, leafletData)
		{
    		angular.extend($scope, {
				prague: {
					lat: 50.067869,
					lng: 14.441749,
					zoom: 11
				}
			});
			leafletData.getMap().then(map => {
			    var resolutions =
			    [
			        198.43789687579377,
			        132.2919312505292,
			        92.60435187537043,
			        52.91677250021167,
			        26.458386250105836,
			        19.843789687579378,
			        13.229193125052918,
			        6.614596562526459,
			        5.291677250021167,
			        3.9687579375158752,
			        2.6458386250105836,
			        1.9843789687579376,
			        1.3229193125052918,
			        0.9260435187537043,
			        0.5291677250021167,
			        0.39687579375158755,
			        0.26458386250105836,
			        0.19843789687579377,
			        0.13229193125052918
			    ];
			    var crs = new L.Proj.CRS("EPSG:5514", "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=589,76,480,0,0,0,0 +units=m +no_defs", {
			        origin: [-33699800, 33699800],
			        resolutions: resolutions
                });
///=============================================== Maps ====================================================================///
   
  ///----------------------------------------- Overlay Maps --------------------------------------------------------------///                
			    var historicFloodFeatureLayer: any = new L.esri.featureLayer({
			        url: "http://mpp.praha.eu/arcgis/rest/services/TI/Historicke_povodne/MapServer/0"
			    });
			    var antiFloodLineFeatureLayer: any = new L.esri.featureLayer({
			        url: "http://mpp.praha.eu/arcgis/rest/services/TI/Zaplavy/MapServer/2"
			    });
                var antiFloodSandLineFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/TI/Zaplavy/MapServer/1",
                    style: (): { color: string } => { return { color: "green" }; }
                });
                var districtHeadquartersFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/4",
                    style: (): { color: string } => { return { color: "blue", fillOpacity: 0  }; }
                });
                var districtHeadquartersPoliceFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/5",
                    style: (): { color: string } => { return { color: "black", fillOpacity: 0  }; }
                });
                var policeDepartmentsFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/7",
                    style: (): { color: string; } => { return { color: "#E16D00", fillOpacity: 0 }; }
                });
                var departmentsOfPoliceFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/6",
                    style: (): { color: string } => { return { color: "yellow", fillOpacity: 0 }; }
                });
                var labels = {};
                districtHeadquartersPoliceFeatureLayer.on("createfeature", e => {
                    var id = e.feature.id;
                    var feature = districtHeadquartersPoliceFeatureLayer.getFeature(id);
                    var center = feature.getBounds().getCenter();
                    var label = L.marker(center, {
                        icon: L.divIcon({
                            iconSize: null,
                            className: "label",
                            html: '<h1>' + e.feature.properties.NÁZEV + '</h1>'
                        })
                    }).addTo(map);
                    labels[id] = label;
                });
                districtHeadquartersPoliceFeatureLayer.on("addfeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        label.addTo(map);
                    }
                });
                districtHeadquartersPoliceFeatureLayer.on("removefeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        map.removeLayer(label);
                    }
                });
                districtHeadquartersFeatureLayer.on("createfeature", e => {
                  var id = e.feature.id;
                    var feature = districtHeadquartersFeatureLayer.getFeature(id);;
                    var center = feature.getBounds().getCenter();
                    var label = L.marker(center, {
                        icon: L.divIcon({
                            iconSize: null,
                            className: "label",
                            html: '<h1>' + e.feature.properties.NÁZEV + '</h1>'
                        })
                    }).addTo(map);
                    labels[id] = label;
                });
                districtHeadquartersFeatureLayer.on("addfeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        label.addTo(map);
                    }
                });
                districtHeadquartersFeatureLayer.on("removefeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        map.removeLayer(label);
                    }
                });
                departmentsOfPoliceFeatureLayer.on("createfeature", e => {
                  var id = e.feature.id;
                  var feature = departmentsOfPoliceFeatureLayer.getFeature(id);;
                    var center = feature.getBounds().getCenter();
                    var label = L.marker(center, {
                        icon: L.divIcon({
                            iconSize: null,
                            className: "label",
                            html: '<h1>' + e.feature.properties.NÁZEV + '</h1>'
                        })
                    }).addTo(map);
                    labels[id] = label;
                });
                departmentsOfPoliceFeatureLayer.on("addfeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        label.addTo(map);
                    }
                });
                departmentsOfPoliceFeatureLayer.on("removefeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        map.removeLayer(label);
                    }
                });
                policeDepartmentsFeatureLayer.on("createfeature", e => {
                  var id = e.feature.id;
                  var feature = policeDepartmentsFeatureLayer.getFeature(id);;
                    var center = feature.getBounds().getCenter();
                    var label = L.marker(center, {
                        icon: L.divIcon({
                            iconSize: null,
                            className: "label",
                            html: '<h1>' + e.feature.properties.KOD + '</h1>'
                        })
                    }).addTo(map);
                    labels[id] = label;
                });
                policeDepartmentsFeatureLayer.on("addfeature", e => {
                    var label = labels[e.feature.id];
                    if (label) {
                        label.addTo(map);
                    }
                });
                policeDepartmentsFeatureLayer.on("removefeature", e => {
                     var label = labels[e.feature.id];
                    if (label) {
                        map.removeLayer(label);
                    }
                });
                var fireFightingCircuitsFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/8",
                    style: (): { color: string } => { return { color: "#16E5E5",fillOpacity: 0 }; }
                });
			    var activeSmallFeatureLayer: any = new L.esri.featureLayer({
			        url: "http://mpp.praha.eu/arcgis/rest/services/TI/Zaplavy/MapServer/14"
			    });
			    var activeBigFeatureLayer: any = new L.esri.featureLayer({
			        url: "http://mpp.praha.eu/arcgis/rest/services/TI/Zaplavy/MapServer/15"
                });
                var categoryFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/TI/Zaplavy/MapServer/16",
                        style: feature => {
                            var chosenColor;
                            switch (feature.properties.KATEG) {
                            case 1:
                                chosenColor = "#C0C0C0";
                                break;
                            case 2:
                                chosenColor = "#E5FF7F";
                                break;
                            case 3:
                                chosenColor = "#E5AE2D";
                                break;
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                                chosenColor = "#ECC66C";
                                break;
                            default:
                                chosenColor = "#1CA589";
                                break;
                            }
                            return { color: chosenColor };
                        }
                    });
                var floodAreaFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/TI/Zaplavy/MapServer/8",
                        style: (): { color: string } => { return { color: "red", fillOpacity: 0 };}
                    });
                var sirenFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/0",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions;
                            if (geojson.properties.DRUH_SIRENY === "rotační") {
                                markerOptions = { icon: icons.sirenGreen };
                            } else {
                                markerOptions = { icon: icons.sirenRed };
                            }
                            return L.marker(latlng, markerOptions);
                        }
                    });     
                var winterServiceFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/DOP/Zimni_udrzba/MapServer/0",
                        style: (feature) => {
                            var chosenColor;
                            switch (feature.properties.IDT_LEVEL) {
                            case 1:
                            case 51:
                                chosenColor = "#6DC066";
                                break;
                            case 2:
                            case 52:
                                chosenColor = "#BE042C";
                                break;
                            case 3:
                            case 53:
                                chosenColor = "#00CBF3";
                                break;
                            case 4:
                            case 54:
                                chosenColor = "#CCFF00";
                                break;
                            case 5:
                                chosenColor = "#C0C0C0";
                                break;
                            case 8:
                                chosenColor = "#C0C0C0";
                                break;
                            case 9:
                                chosenColor = "#2C5234";
                                break;
                            default:
                                chosenColor = "";
                                break;
                            } 
                            return { color: chosenColor };
                        }
                    });

			    var geojsonSafetyEight: any = SafetyEight;
                var geojsonq2002: any = q2002;
                var geojsonq100: any = q100;
                var geojsonq50: any = q50;
			    var geojsonq20: any = q20;
                var geojsonq10: any = q10; 

                var safetyEightLayer = new L.Proj.GeoJSON(geojsonSafetyEight, {
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                                {
                                    icon: icons.camera
                                };
                            return L.marker(latlng, markerOptions);
                        }
                });
                var q2002Layer = new L.Proj.GeoJSON(geojsonq2002, {
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                                {
                                    icon: icons.circle
                                };
                            return L.marker(latlng, markerOptions);
                        }
                });  
                var q100Layer = new L.Proj.GeoJSON(geojsonq100, {
                    pointToLayer: (geojson, latlng) => {
                        var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.circle
                            };
                        return L.marker(latlng, markerOptions);
                    }
                });  
                var q50Layer = new L.Proj.GeoJSON(geojsonq50, {
                    pointToLayer: (geojson, latlng) => {
                        var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.circle
                            };
                        return L.marker(latlng, markerOptions);
                    }
                });
                var q20Layer = new L.Proj.GeoJSON(geojsonq20, {
                    pointToLayer: (geojson, latlng) => {
                        var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.circle
                            };
                        return L.marker(latlng, markerOptions);
                    }
                });
                var q10Layer = new L.Proj.GeoJSON(geojsonq10, {
                    pointToLayer: (geojson, latlng) => {
                        var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.circle
                            };
                        return L.marker(latlng, markerOptions);
                    }
                });

                var geoJsonData: any = data;
                geoJsonData.crs = {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:5514"
                    }
                };
			    var praha8Layer = new L.Proj.GeoJSON(geoJsonData, {
                    style: (): { color: string } => {
                        return { color: "red", fillOpacity: 0 };
                    }
			});
                var mppobjectsFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/3",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.objectPolice
                            };
                            return L.marker(latlng, markerOptions);
                        }
                    });
                var policeFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/0",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.police
                            };
                            return L.marker(latlng, markerOptions);
                        }
                    });
                var schelderSmallFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/3",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.schelder
                            };
                            return L.marker(latlng, markerOptions);
                        }
                });
                var schelderBigFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/4",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.schelderBig
                            };
                            return L.marker(latlng, markerOptions);
                        }
                });
                var metroFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/7",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                    icon: icons.stick,
                                    iconAngle: geojson.properties.UHEL
                            };
                            return L.marker(latlng, markerOptions);
                        }
                });
                var metroStationsFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/8",
                    pointToLayer: (geojson, latlng) => {
                        var chosenIcon;
                            switch (geojson.properties.TRASA) {
                                case "A":
                                    chosenIcon = icons.greenMetro;
                                    break;
                                case "B":
                                    chosenIcon = icons.yellowMetro;
                                    break;
                                case "C":
                                    chosenIcon = icons.redMetro;
                                    break;
                                default:
                                    chosenIcon = icons.free;
                                    break;
                            }
                            var markerOptions: L.MarkerOptions =
                                {
                                    icon:chosenIcon
                                };
                            return L.marker(latlng, markerOptions);
                        }
                });
                var entersFeatureLayer: any = new L.esri.featureLayer({
                    url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/6",
                    pointToLayer: (geojson, latlng) => {
                        var chosenIcon;
                            switch (geojson.properties.TRASA) {
                                case "A":
                                    chosenIcon = icons.greenEnter;
                                    break;
                                case "B":
                                    chosenIcon = icons.yellowEnter;
                                    break;
                                case "C":
                                    chosenIcon = icons.redEnter;
                                    break;
                                default:
                                    chosenIcon = icons.free;
                                    break;
                            }
                            var markerOptions: L.MarkerOptions =
                                {
                                    icon:chosenIcon
                                };
                            return L.marker(latlng, markerOptions);
                        }
                });

            var metroTunelsFeatureLayer: any = new L.esri.featureLayer({
                url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/9",

                    style: (feature) => {
                        var chosenColor;
                        switch (feature.properties.TRASA) {
                            case "A":
                                chosenColor = "#458902";
                                break;
                            case "B":
                                chosenColor = "#FFF400";
                                break;
                            case "C":
                                chosenColor = "#FF073A";
                                break;
                            default:
                                chosenColor = "#009ACD";
                                break;
                        }
                        return {
                            color: chosenColor,
                            weight: 12,
                            opacity: 1,
                            className:feature.properties.UKRYT_USEK
                        };
                    }
                });
                var emergencyFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/1",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.ambulance
                            };
                            return L.marker(latlng, markerOptions);
                        }
                    });
                var noFireFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/Integrovany_zachranny_system/MapServer/2",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.firemen
                            };
                            return L.marker(latlng, markerOptions);
                        }
                    });
                var alertFeatureLayer: any = new L.esri.featureLayer({
                        url: "http://mpp.praha.eu/arcgis/rest/services/FS/ukryti_varovani_objekty_s_toxickymi_latkami/MapServer/1",
                        pointToLayer: (geojson, latlng) => {
                            var markerOptions: L.MarkerOptions =
                            {
                                icon: icons.hazardousWaste
                            };
                            return L.marker(latlng, markerOptions);
                        }
                    });

///------------------------------------------------ Base Maps --------------------------------------------------///
			    var tiledMap: any = new L.esri.tiledMapLayer({
			        url: "http://mpp.praha.eu/arcgis/rest/services/DMP/MTVU/MapServer",
			        maxZoom: 18,
			        minZoom: 0,
			        continuousWorld: true
			    });
			    var flyMap: any = new L.esri.tiledMapLayer({
			        url: "http://wgp.urm.cz/ArcGIS/rest/services/MAP/letecke_snimky_posledni_snimkovani_cache/MapServer",
			        maxZoom: 16,
			        minZoom: 0,
			        continuousWorld: true
                });

///====================================================== implementations ================================================///
                    var baseMaps = {
                        "Základní mapa": tiledMap,
                        "Letecká mapa": flyMap
                    };
			    var overlayMaps = {
			        "Historické povodně - povodeň 2013": historicFloodFeatureLayer,
			        "Objekty Policie ČR ": policeFeatureLayer,
			        "Objekty Zdravotnické záchranné služby hl. m. Prahy": emergencyFeatureLayer,
			        "Objekty požární ochrany": noFireFeatureLayer,
			        "Aktivní zóna na drobných vodních tocích": activeSmallFeatureLayer,
			        "Aktivní zóna": activeBigFeatureLayer,
			        "Protipovodňová ochrana": antiFloodLineFeatureLayer,
			        "Objekty s nebezpečnými látkami": alertFeatureLayer,
			        "Pytle s pískem ": antiFloodSandLineFeatureLayer,
			        "Sirény": sirenFeatureLayer,
			        "Úkryty s kapacitou menší než 150 lidí": schelderSmallFeatureLayer,
			        "Úkryty s kapacitou 151 lidí a více": schelderBigFeatureLayer,
			        "Zimní údržba komunikací": winterServiceFeatureLayer,
			        "Objekty Městské policie Praha - body": mppobjectsFeatureLayer,
			        "Kategorie záplavových území Vltavy a Berounky": categoryFeatureLayer,
			        "Záplavová území pro Qn (Vltava, Berounka)": floodAreaFeatureLayer,
			        "Městská část Praha 8": praha8Layer,
			        "Adresy ohrožených budov - Q2002": q2002Layer,
			        "Adresy ohrožených budov - Q100": q100Layer,
			        "Adresy ohrožených budov - Q50": q50Layer,
			        "Adresy ohrožených budov - Q20": q20Layer,
                    "Adresy ohrožených budov - Q10": q10Layer,
                    "Ochranný systém metra - tunely": metroTunelsFeatureLayer,
                    "Ochranný systém metra - stanice": metroStationsFeatureLayer,
                    "Ochranný systém metra - vstupy": entersFeatureLayer,
                    "Hranice úseků ochran. syst. metra": metroFeatureLayer,
                    "Bezpečná osmička": safetyEightLayer,
                    "Obvodní ředitelství Městské policie Praha": districtHeadquartersFeatureLayer,
                    "Obvodní ředitelství Policie ČR": districtHeadquartersPoliceFeatureLayer,
                    "Okrsky Městské policie Praha": departmentsOfPoliceFeatureLayer,
                    "Okrsky policie ČR": policeDepartmentsFeatureLayer,
                    "Hasební obvody": fireFightingCircuitsFeatureLayer
			};

                    L.control.layers(baseMaps,overlayMaps).addTo(map);
                    tiledMap.options.tileSize = 512;
                    tiledMap.addTo(map);
                    flyMap.options.tileSize = 512;
                    map.options.crs = crs;
                    map.setView([50.067869, 14.441749], 11);
                }); 
			
		}
	}
}