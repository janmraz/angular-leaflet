﻿var q20 = {
    "type": "FeatureCollection",

    "features": [
        { "type": "Feature", "properties": { "Q10": 0.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22375481, "Adresa": "U Českých loděnic 40\/6" }, "geometry": { "type": "Point", "coordinates": [14.469167837224528, 50.107458049243313] } },
        { "type": "Feature", "properties": { "Q10": 1.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22319221, "Adresa": "V Zámcích 313\/46" }, "geometry": { "type": "Point", "coordinates": [14.397571110259458, 50.133107306287386] } },
        { "type": "Feature", "properties": { "Q10": 0.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22748091, "Adresa": "Koželužská 600\/2" }, "geometry": { "type": "Point", "coordinates": [14.471083595632498, 50.105559578767938] } },
        { "type": "Feature", "properties": { "Q10": 1.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22394605, "Adresa": "Libeňský ostrov 528" }, "geometry": { "type": "Point", "coordinates": [14.458817725510237, 50.111624499849121] } },

        { "type": "Feature", "properties": { "Q10": 0.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22376283, "Adresa": "Vojenova 175\/7" }, "geometry": { "type": "Point", "coordinates": [14.472250301332672, 50.104898389209573] } }
    ]
}
