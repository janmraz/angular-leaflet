﻿var q10 = {
    "type": "FeatureCollection",

    "features": [
        { "type": "Feature", "properties": { "Q10": 1.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22319221, "Adresa": "V Zámcích 313\/46" }, "geometry": { "type": "Point", "coordinates": [14.397571110259458, 50.133107306287386] } },
        { "type": "Feature", "properties": { "Q10": 1.000000, "Q20": 1, "Q50": 1, "Q100": 1, "Q2002": 1, "Kod_adresa": 22394605, "Adresa": "Libeňský ostrov 528" }, "geometry": { "type": "Point", "coordinates": [14.458817725510237, 50.111624499849121] } }
    ]
}
