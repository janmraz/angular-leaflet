﻿/// <reference path="Scripts/typings/angularjs/angular.d.ts" />
/// <reference path="Home/HomeController.ts" />
/// <reference path="Home/IHomeScope.ts" />

module Intens.TestApp {
    export class App {
        private m_appModule: ng.IModule;

        constructor() {
            this.m_appModule = angular.module("TestApp", ["ui.router", "leaflet-directive"]);
            this.setupControllers();
            this.setupFilters();

            this.m_appModule.config(($stateProvider, $urlRouterProvider) => {
                $urlRouterProvider.otherwise("/home");
                $stateProvider.state("home", {
                    url: "/home",
                    templateUrl: "Home/HomeView.html"
                });
            });
        }
        
        private setupFilters() {
        }

        private setupControllers() {
            this.m_appModule.controller("HomeController", Home.HomeController);
        }


        public start() {
            $(document).ready(() => {
                console.log("booting " + "IsipApp");
                angular.bootstrap(document, [this.m_appModule.name]);
            });
        }
		//public start() {
        //    console.log("booting " + "IsipApp");
		//	angular.bootstrap(document, [this.m_appModule.name]);
        //}
    }

    $(document).ready(() => {
        new App().start();
    });
	//new App().start();
}